Ejercicio 1
El codigo consiste en la creacion de una tabla de 10x10 que contiene los numeros del 1 al 100.

Esta tabla puedo ser creada gracias a dos for en donde se le iba sumando 1 a cada numero para poder crear la tabla y luego se saltaba a la fila siguiente cuando llegaba a sumar 10 numeros.

Para ver si el codigo funciona se debe activar apache2 y luego acceder a la carpeta public_html contenedora de una carpeta llamada "lab1-2-tallerweb" para luego acceder a la carpeta ejercicio1.
