Ejercicio 2 -Laboratorio 1 -Unidad 2

Para poder crear una tabla de tamaño NxN de 1 a NxN con sus filas de color gris y blanco alternadas.
El primer paso fue definir los valores de los tamaños con las variable $TAMy luego se utilizaron dos for que permiten la creacion del ancho y largo de la fila, como en el ejercicio anterior.

Para darle colores alternados entre gris y blanco a las filas se hicieron cambios en el estilo css especificamente con tr.

Para ver si el codigo funciona se debe activar apache2 y luego acceder a la carpeta public_html contenedora de una carpeta llamada "lab1-2-tallerweb" para luego acceder a la carpeta ejercicio2.
