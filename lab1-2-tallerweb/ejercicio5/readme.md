Ejercicio 5 -Laboratorio 1- Unidad 2

Para la creacion de este ejercicio primero se indica cual es el directorio contenedor de los archivos a mostrar.
Luego se abre la carpeta y se crea un for que le va indicando al programa el tamaño de la columna, en este caso 4.
Luego hay un while que permite ir leyendo cada uno de los archivos para luego imprimir las imagenes en cada cuadrado.
Se cierra la carpeta con close().
Para ver si el codigo funciona se debe activar apache2 y luego acceder a la carpeta public_html contenedora de una carpeta llamada "lab1-2-tallerweb" para luego acceder a la carpeta ejercicio5.

